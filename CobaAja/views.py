from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse

def index(request):
    riwayat = ['TKIT Ummul Quro', 'SDIT Ummul Quro', 'SMPIT Ummul Quro', 'SMAN 5 Bogor', 'Universitas Indonesia']
    context =  {
        'nama' : 'Hasna Nadifah',
        'NPM' : '1906293096',
        'title' : 'About Me',
        'riwayat' : riwayat,
    }
    return render(request, 'CobaAja.html', context)