
from django.shortcuts import render

def index(request):
    return render(request, 'index.html')

def aboutme(request):
    biodata = ['Name: Hasna Nadifah', 'Status: Student', 'NPM: 1906293096', 'Year: 2019', 'Hobby: Watching Movies', 'Email: hnadifah@gamil.com' ]
    context = {
        'title' : 'About Me',
        'biodata' : biodata
    }
    return render(request, 'AboutMe.html', context)

def experiences(request):
    return render(request, 'Experiences.html')

def favourites(request):
    return render(request, 'Favourites.html')