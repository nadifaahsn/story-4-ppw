from django.contrib import admin
from django.urls import path, include

from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index),
    path('AboutMe/', views.aboutme),
    path('Experiences/', views.experiences),
    path('Favourites/', views.favourites),
    path('story1/', include('CobaAja.urls')),
    
]
